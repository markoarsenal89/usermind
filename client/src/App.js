import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import Home from "./views/Home";
import ReportSale from "./views/ReportSale";

function App() {
  return (
    <div className="pg-container">
      <Router>
        <div>
          <nav>
            <ul className="nav-list">
              <li>
                <NavLink to="/" activeClassName="active" exact={true}>
                  Home
                </NavLink>
              </li>
              <li>|</li>
              <li>
                <NavLink
                  to="/report-sale"
                  activeClassName="active"
                  exact={true}
                >
                  Report sale
                </NavLink>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route path="/report-sale">
              <ReportSale />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
