import React, { useState, useEffect } from "react";
import loadingImg from "../assets/images/loading.svg";
import { server } from "../utils";

const SaleForm = ({ date }) => {
  const initialState = {
    date,
    bananas: 0,
    strawberries: 0,
    apples: 0,
    oranges: 0
  };
  const [formData, setFormData] = useState(initialState);
  const [loading, setLoading] = useState(false);

  const formChangeHandler = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  useEffect(() => {
    server(null, encodeURIComponent(JSON.stringify([date]))).then(data => {
      if (Object.entries(data).length) {
        setFormData(data[date]);
      } else {
        setFormData({
          ...initialState,
          date
        });
      }
    });
  }, [date]);

  const formSubmitHandler = e => {
    e.preventDefault();

    setLoading(true);
    server({
      body: JSON.stringify(formData),
      method: "POST"
    }).then(data => {
      setTimeout(() => {
        setLoading(false);
      }, 200);
    });
  };

  return (
    <form className="sales-form" onSubmit={formSubmitHandler}>
      <div className="form-group">
        <label className="control-label">date</label>
        <input
          type="text"
          className="form-control"
          name="date"
          id="date"
          value={formData.date}
          onChange={formChangeHandler}
          disabled
        />
      </div>
      <div className="form-group">
        <label className="control-label">bananas</label>
        <input
          type="number"
          className="form-control"
          name="bananas"
          id="bananas"
          value={formData.bananas}
          onChange={formChangeHandler}
        />
      </div>
      <div className="form-group">
        <label className="control-label">strawberries</label>
        <input
          type="number"
          className="form-control"
          name="strawberries"
          id="strawberries"
          value={formData.strawberries}
          onChange={formChangeHandler}
        />
      </div>
      <div className="form-group">
        <label className="control-label">apples</label>
        <input
          type="number"
          className="form-control"
          name="apples"
          id="apples"
          value={formData.apples}
          onChange={formChangeHandler}
        />
      </div>
      <div className="form-group">
        <label className="control-label">oranges</label>
        <input
          type="number"
          className="form-control"
          name="oranges"
          id="oranges"
          value={formData.oranges}
          onChange={formChangeHandler}
        />
      </div>
      <button className="btn btn-green save-btn">
        save
        <img
          src={loadingImg}
          alt="loading..."
          className="loader"
          style={{ display: loading ? "inline" : "none" }}
        />
      </button>
    </form>
  );
};

export default SaleForm;
