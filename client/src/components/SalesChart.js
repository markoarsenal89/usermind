import React from "react";

const SalesChart = ({ data }) => {
  if (!data || Object.entries(data).length === 0) {
    return null;
  }

  const maxTotal = Object.entries(data).reduce((max, [key, { total }]) => {
    return total > max ? total : max;
  }, 0);

  return (
    <div className="charts-wrapper">
      {Object.entries(data).map(
        ([key, { date, bananas, strawberries, apples, oranges, total }]) => {
          return (
            <div className="chart-wrapper" key={date}>
              <h4 className="chart-date">{date}</h4>
              <div className="chart-data">
                <div className="data-box">
                  <div className="data-label">
                    total - <span className="sales">{total}</span>
                  </div>
                  <div
                    className="chart-bar total-color"
                    style={{
                      width: `${(total / maxTotal) * 100}%`,
                      height: `${(total / maxTotal) * 200}px`
                    }}
                  ></div>
                </div>
                <div className="data-box">
                  <div className="data-label">
                    bananas - <span className="sales">{bananas}</span>
                  </div>
                  <div
                    className="chart-bar bananas-color"
                    style={{
                      width: `${(bananas / maxTotal) * 100}%`,
                      height: `${(bananas / maxTotal) * 200}px`
                    }}
                  ></div>
                </div>
                <div className="data-box">
                  <div className="data-label">
                    strawberries - <span className="sales">{strawberries}</span>
                  </div>
                  <div
                    className="chart-bar strawberries-color"
                    style={{
                      width: `${(strawberries / maxTotal) * 100}%`,
                      height: `${(strawberries / maxTotal) * 200}px`
                    }}
                  ></div>
                </div>
                <div className="data-box">
                  <div className="data-label">
                    apples - <span className="sales">{apples}</span>
                  </div>
                  <div
                    className="chart-bar apples-color"
                    style={{
                      width: `${(apples / maxTotal) * 100}%`,
                      height: `${(apples / maxTotal) * 200}px`
                    }}
                  ></div>
                </div>
                <div className="data-box">
                  <div className="data-label">
                    oranges - <span className="sales">{oranges}</span>
                  </div>
                  <div
                    className="chart-bar oranges-color"
                    style={{
                      width: `${(oranges / maxTotal) * 100}%`,
                      height: `${(oranges / maxTotal) * 200}px`
                    }}
                  ></div>
                </div>
              </div>
            </div>
          );
        }
      )}
    </div>
  );
};

export default SalesChart;
