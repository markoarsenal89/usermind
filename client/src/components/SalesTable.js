import React, { Fragment, useState, useEffect } from "react";

const SalesTable = ({ data }) => {
  const [rerender, setRerender] = useState(0);

  useEffect(() => {
    const checkSize = () => {
      if (window.matchMedia("(min-width: 768px)").matches) {
        setRerender(1);
      } else {
        setRerender(0);
      }
    };

    window.addEventListener("resize", checkSize);

    return () => {
      window.removeEventListener("resize", checkSize);
    };
  }, []);

  if (!data || Object.entries(data).length === 0) {
    return (
      <div>
        No data for the selected date{" "}
        <span role="img" aria-label="confused face emoji">
          😕
        </span>
      </div>
    );
  }

  return window.matchMedia("(min-width: 768px)").matches ? (
    <table className="table" key={rerender}>
      <thead>
        <tr>
          <th>date</th>
          <th>bananas</th>
          <th>strawberries</th>
          <th>apples</th>
          <th>oranges</th>
        </tr>
      </thead>
      <tbody>
        {Object.entries(data).map(
          ([key, { date, bananas, strawberries, apples, oranges }]) => {
            console.log(date);

            return (
              <tr key={date}>
                <td>{date}</td>
                <td>{bananas}</td>
                <td>{strawberries}</td>
                <td>{apples}</td>
                <td>{oranges}</td>
              </tr>
            );
          }
        )}
      </tbody>
    </table>
  ) : (
    <Fragment key={rerender}>
      {Object.entries(data).map(
        ([key, { date, bananas, strawberries, apples, oranges }]) => {
          return (
            <table className="table" key={date}>
              <tbody>
                <tr>
                  <th>date</th>
                  <th>{date}</th>
                </tr>
                <tr>
                  <td>bananas</td>
                  <td>{bananas}</td>
                </tr>
                <tr>
                  <td>strawberries</td>
                  <td>{strawberries}</td>
                </tr>
                <tr>
                  <td>apples</td>
                  <td>{apples}</td>
                </tr>
                <tr>
                  <td>oranges</td>
                  <td>{oranges}</td>
                </tr>
              </tbody>
            </table>
          );
        }
      )}
    </Fragment>
  );
};

export default SalesTable;
