import React, { useState, Fragment } from "react";
import { Calendar } from "react-date-range";
import SaleForm from "../components/SaleForm";
import moment from "moment";
import { DATE_FORMAT } from "../constants";

const ReportSale = () => {
  const [date, setDate] = useState(moment());

  return (
    <Fragment>
      <div className="date-picker-wrapper">
        <Calendar
          date={date}
          onChange={d => {
            setDate(moment(d));
          }}
        />
      </div>
      <SaleForm date={date.format(DATE_FORMAT)} />
    </Fragment>
  );
};

export default ReportSale;
