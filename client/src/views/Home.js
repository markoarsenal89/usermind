import React, { useState, Fragment, useEffect } from "react";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import { DateRange } from "react-date-range";
import SalesTable from "../components/SalesTable";
import SalesChart from "../components/SalesChart";
import { server, enumerateDaysBetweenDates } from "../utils";
import { DATE_FORMAT } from "../constants";
import moment from "moment";

const Home = () => {
  const initialRange = {
    startDate: moment(),
    endDate: moment(),
    key: "selection"
  };

  const [range, setRange] = useState(initialRange);
  const [data, setData] = useState(null);

  useEffect(() => {
    const dates = enumerateDaysBetweenDates(range.startDate, range.endDate).map(
      d => {
        return moment(d).format(DATE_FORMAT);
      }
    );

    server(null, encodeURIComponent(JSON.stringify(dates))).then(res => {
      setData(res);
    });
  }, [range]);

  return (
    <Fragment>
      <div className="date-picker-wrapper">
        <DateRange
          className="date-picker"
          ranges={[range]}
          onChange={({ selection, range1 }) => {
            const dates = selection || range1;
            setRange({
              startDate: moment(dates.startDate),
              endDate: moment(dates.endDate)
            });
          }}
        />
      </div>
      <SalesTable data={data} />
      <SalesChart data={data} />
    </Fragment>
  );
};

export default Home;
