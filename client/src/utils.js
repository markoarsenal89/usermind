import { SERVER } from "./constants";
import moment from "moment";

export const server = (options, urlParams) => {
  return new Promise((resolve, reject) => {
    fetch(`${SERVER}/data/${urlParams ? urlParams : ""}`, {
      headers: {
        "Content-Type": "application/json"
      },
      ...options
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
        throw new Error(err);
      });
  });
};

export const enumerateDaysBetweenDates = (startDate, endDate) => {
  const currDate = moment(startDate).startOf("day");
  const lastDate = moment(endDate).endOf("day");

  const dates = [currDate.clone().toDate()];

  while (currDate.add(1, "days").diff(lastDate) < 0) {
    dates.push(currDate.clone().toDate());
  }

  return dates;
};
