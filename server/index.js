const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const fs = require("fs");

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.listen(3001, () => {
  console.log("Listening on http://localhost:3001");
});

app.get("/", (req, res) => {
  res.end("Hello, good sir/ma'am.\nI am your server.\nHow can I help you?");
});

app.get("/data/:dates", (req, res) => {
  const { dates } = req.params;
  const parsedDates = JSON.parse(dates);

  fs.readFile("./data.json", (err, data) => {
    if (err) {
      res.status(400);
      res.send(err);
      res.end();
    } else {
      const parsedData = JSON.parse(data);
      const responseData = {};

      for (const k in parsedDates) {
        responseData[parsedDates[k]] = parsedData[parsedDates[k]];
      }

      res.status(200);
      res.json(Object.entries(responseData).length ? responseData : null);
      res.end();
    }
  });
});

app.post("/data", (req, res) => {
  const data = req.body;

  fs.readFile("./data.json", (err, allData) => {
    if (err) {
      res.status(400);
      res.send(err);
      res.end();
    } else {
      const parsedData = JSON.parse(allData);

      data.total =
        parseInt(data.bananas) +
        parseInt(data.strawberries) +
        parseInt(data.apples) +
        parseInt(data.oranges);
      parsedData[data.date] = data;

      fs.writeFile("./data.json", JSON.stringify(parsedData, null, 2), () => {
        res.status(200);
        res.json(data);
        res.end();
      });
    }
  });
});
